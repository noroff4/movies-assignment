﻿using Microsoft.EntityFrameworkCore;
using movies_assignment.Data;
using movies_assignment.Models;

namespace movies_assignment.Models
{
    public class MovieDbContext : DbContext
    {
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Character> Character { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MovieDbContext(DbContextOptions<MovieDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>().ToTable("Movie");
            modelBuilder.Entity<Character>().ToTable("Character");
            modelBuilder.Entity<Franchise>().ToTable("Franchise");

            // Character seed
            modelBuilder.Entity<Character>().HasData(SeedHelper.SeedCharacters());
            // Franchise seed
            modelBuilder.Entity<Franchise>().HasData(SeedHelper.SeedFranchises());
            // Movie seed
            modelBuilder.Entity<Movie>().HasData(SeedHelper.SeedMovies());

            //many to many (character <-> movie) seed
            modelBuilder.Entity<Movie>()
              .HasMany(p => p.Characters)
              .WithMany(m => m.Movies)
              .UsingEntity<Dictionary<string, object>>(
                  "MovieCharacters",
                  r => r.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                  l => l.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                  je =>
                  {
                      je.HasKey("MovieId", "CharacterId");
                      je.HasData(
                          new { MovieId = 1, CharacterId = 1 },
                          new { MovieId = 1, CharacterId = 2 },
                          new { MovieId = 1, CharacterId = 3 },
                          new { MovieId = 2, CharacterId = 1 },
                          new { MovieId = 2, CharacterId = 2 },
                          new { MovieId = 2, CharacterId = 3 },
                          new { MovieId = 3, CharacterId = 1 },
                          new { MovieId = 3, CharacterId = 2 },
                          new { MovieId = 3, CharacterId = 3 },
                          new { MovieId = 4, CharacterId = 2 }

                      );
                  });



        }

    }
}
