﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace movies_assignment.Models
{
    public class Movie
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int MovieId { get; set; }
        
        [MaxLength(64)]
        public string Title { get; set; }
        [MaxLength(255)]
        public string? Genre { get; set; }
        public int ReleaseYear { get; set; }
        [MaxLength(255)]
        public string? Director { get; set; }
        [MaxLength(255)]
        public string? Picture { get; set; }
        [MaxLength(255)]
        public string? Trailer { get; set; }
        public int FranchiseId { get; set; }
        public Franchise? Franchise { get; set; }
        public ICollection<Character>? Characters { get; set; }
        
    }
}
