﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace movies_assignment.Models
{
    public class Character
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CharacterId { get; set; }
        [MaxLength(255)]
        public string FullName { get; set; }
        [MaxLength(255)]
        public string Alias { get; set; }
        [MaxLength(64)]
        public string Gender { get; set; }
        [MaxLength(255)]
        public string? PictureURL { get; set; }
        public ICollection<Movie>? Movies { get; set; }
    }
}
