﻿using movies_assignment.DTOs.MovieDTOs;
using movies_assignment.Models;
using AutoMapper;

namespace movies_assignment.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(cdto => cdto.Characters, opt => opt
                .MapFrom(c => c.Characters.Select(c => c.CharacterId).ToArray()));

            CreateMap<MovieCreateDTO, Movie>();

            CreateMap<MovieEditDTO, Movie>();
        }
    }
}