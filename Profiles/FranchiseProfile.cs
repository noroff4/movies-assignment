﻿using AutoMapper;
using movies_assignment.DTOs.FranchiseDTOs;
using movies_assignment.Models;

namespace movies_assignment.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(c => c.MovieId).ToArray()));

            CreateMap<FranchiseCreateDTO, Franchise>();

            CreateMap<FranchiseEditDTO, Franchise>();
        }
    }
}
