﻿using AutoMapper;
using movies_assignment.DTOs.CharacterDTOs;
using movies_assignment.Models;

namespace movies_assignment.Profiles
{
    public class CharacterProfile : Profile
    {

        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(c => c.MovieId).ToArray()));

            CreateMap<CharacterCreateDTO, Character>();

            CreateMap<CharacterEditDTO, Character>();
        }
    }
}
