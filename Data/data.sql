INSERT INTO Character (FullName, Alias, Gender)
VALUES ('Steven Stromboulopoulos', 'Stetu', 'Male');

INSERT INTO Character (FullName, Alias, Gender)
VALUES ('Eppo Epposan', 'Eb', 'Male');

INSERT INTO Character (FullName, Alias, Gender)
VALUES ('Roope R�pelt�j�', 'Benos', 'Male');

INSERT INTO Franchise([Name], [Description])
VALUES ('Oumuamua', 'NHL Seikkailut');

insert into Movie (Title, Genre, ReleaseYear, FranchiseId) values ('Nhl 19', 'Sports', 2019, (select FranchiseId from Franchise as fr where fr.[Name] = 'Oumuamua'))
insert into Movie (Title, Genre, ReleaseYear, FranchiseId) values ('Nhl 20', 'Sports', 2020, (select FranchiseId from Franchise as fr where fr.[Name] = 'Oumuamua'))
insert into Movie (Title, Genre, ReleaseYear, FranchiseId) values ('Nhl 21', 'Sports', 2021, (select FranchiseId from Franchise as fr where fr.[Name] = 'Oumuamua'))


INSERT INTO MovieCharacter (MovieId, CharacterId) values ((select MovieId from Movie as mv where mv.Title='Nhl 19'), (select CharacterId from [Character] as ch where ch.Alias = 'Stetu'));
INSERT INTO MovieCharacter (MovieId, CharacterId) values ((select MovieId from Movie as mv where mv.Title='Nhl 20'), (select CharacterId from [Character] as ch where ch.Alias = 'Stetu'));
INSERT INTO MovieCharacter (MovieId, CharacterId) values ((select MovieId from Movie as mv where mv.Title='Nhl 21'), (select CharacterId from [Character] as ch where ch.Alias = 'Stetu'));

INSERT INTO MovieCharacter (MovieId, CharacterId) values ((select MovieId from Movie as mv where mv.Title='Nhl 19'), (select CharacterId from [Character] as ch where ch.Alias = 'Eb'));
INSERT INTO MovieCharacter (MovieId, CharacterId) values ((select MovieId from Movie as mv where mv.Title='Nhl 20'), (select CharacterId from [Character] as ch where ch.Alias = 'Eb'));
INSERT INTO MovieCharacter (MovieId, CharacterId) values ((select MovieId from Movie as mv where mv.Title='Nhl 21'), (select CharacterId from [Character] as ch where ch.Alias = 'Eb'));

INSERT INTO MovieCharacter (MovieId, CharacterId) values ((select MovieId from Movie as mv where mv.Title='Nhl 21'), (select CharacterId from [Character] as ch where ch.Alias = 'Benos'));