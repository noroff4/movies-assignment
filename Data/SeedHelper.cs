﻿using movies_assignment.Models;

namespace movies_assignment.Data
{
    public static class SeedHelper
    {

        public static ICollection<Franchise> SeedFranchises()
        {
            var franchises = new List<Franchise>();
            franchises.Add(new Franchise { FranchiseId = 1, Name = "Star Wars", Description = "Far away galaxy" });
            franchises.Add(new Franchise { FranchiseId = 2, Name = "Lord of the rings", Description = "Rings and orcs" });

            return franchises;
        }

        public static ICollection<Movie> SeedMovies()
        {
            var movies = new List<Movie>();
            movies.Add(new Movie() { MovieId = 1, Title = "A New Hope", FranchiseId = 1, Director = "starwars director", Genre = "scifi", ReleaseYear = 1990 });
            movies.Add(new Movie() { MovieId = 2, Title = "Two Towers", FranchiseId = 2, Director = "lotr director", Genre = "fantasy", ReleaseYear = 2000 });
            movies.Add(new Movie() { MovieId = 3, Title = "Return of the jedi", FranchiseId = 1, Director = "starwars director", Genre = "scifi", ReleaseYear = 2015 });
            movies.Add(new Movie() { MovieId = 4, Title = "Revenge of the sith", FranchiseId = 1, Director = "starwars director", Genre = "scifi", ReleaseYear = 1986 });

            return movies;
        }

        public static ICollection<Character> SeedCharacters()
        {
            var characters = new List<Character>();
            characters.Add(new Character { CharacterId = 1, FullName = "Steven Stromboulopoulos", Alias = "Stetu", Gender = "Male" });
            characters.Add(new Character { CharacterId = 2, FullName = "Eppo Epposan", Alias = "Epposan", Gender = "Male" });
            characters.Add(new Character { CharacterId = 3, FullName = "Roope Räpeltäjä", Alias = "Benos", Gender = "Male" });

            return characters;
        }

    }
}
