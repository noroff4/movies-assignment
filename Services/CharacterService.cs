﻿using Microsoft.EntityFrameworkCore;
using movies_assignment.Models;

namespace movies_assignment.Services
{   /// <summary>
    /// Services are used so controllers have less actions. Database interaction happens here
    /// </summary>
    #pragma warning disable 1591
    public class CharacterService : ICharacterService
    {

        private readonly MovieDbContext _context;

        public CharacterService(MovieDbContext context)
        {
            _context = context;
        }

        public async Task<Character> AddCharacterAsync(Character character)
        {
            _context.Character.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }

        public bool CharacterExists(int id)
        {
            return _context.Character.Any(e => e.CharacterId == id);
        }

        public async Task DeleteCharacterAsync(int id)
        {
            var character = await _context.Character.FindAsync(id);
            if (character != null) _context.Character.Remove(character);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Character>> GetAllCharactersAsync()
        {
            return await _context.Character.Include(c => c.Movies).ToListAsync();
        }

        public async Task<Character> GetSpecificCharacterAsync(int id)
        {
            return await _context.Character.Include(c => c.Movies).FirstAsync(c => c.CharacterId == id);
                    
        }

        public async Task UpdateCharacterMoviesAsync(int characterId, List<int> movies)
        {

            Character CharaterToUpdateMovies = await _context.Character
                .Include(c => c.Movies)
                .Where(c => c.CharacterId == characterId)
                .FirstAsync();


            List<Movie> _movies = new();
            foreach (int movieId in movies)
            {
                Movie? movie = await _context.Movies.FindAsync(movieId);
                if (movie == null)
                {
                    throw new KeyNotFoundException();
                }

                _movies.Add(movie);
            }
            CharaterToUpdateMovies.Movies = _movies;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateCharacterAsync(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
#pragma warning restore 1591
}
