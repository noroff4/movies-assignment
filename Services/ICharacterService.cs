﻿using movies_assignment.Models;

namespace movies_assignment.Services
{/// <summary>
 /// Services are used so controllers have less actions. Database interaction happens here
 /// </summary>
    public interface ICharacterService
    {
        /// <summary>
        /// Asynchronous method to get all characters from database
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Character>> GetAllCharactersAsync();

        /// <summary>
        /// Asynchronous method to get specific character from database
        /// </summary>
        /// <param name="id">ID of the character</param>
        /// <returns></returns>
        public Task<Character> GetSpecificCharacterAsync(int id);

        /// <summary>
        /// Asynchronous method to add <paramref name="character"/> to database
        /// </summary>
        /// <param name="character">Character to add</param>
        /// <returns></returns>
        public Task<Character> AddCharacterAsync(Character character);

        /// <summary>
        /// Asynchronous method to update <paramref name="character"/> in database
        /// </summary>
        /// <param name="character">Updated character</param>
        /// <returns></returns>
        public Task UpdateCharacterAsync(Character character);

        /// <summary>
        /// Asynchronous method to update movies the character has
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="movies"></param>
        /// <returns></returns>
        public Task UpdateCharacterMoviesAsync(int characterId, List<int> movies);

        /// <summary>
        /// Asynchronous method to delete character from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task DeleteCharacterAsync(int id);

        /// <summary>
        /// Check if character exists in database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool CharacterExists(int id);

    }
}
