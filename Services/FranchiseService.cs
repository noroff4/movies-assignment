﻿using Microsoft.EntityFrameworkCore;
using movies_assignment.Models;
using NuGet.Protocol;

namespace movies_assignment.Services
{/// <summary>
 /// Services are used so controllers have less actions. Database interaction happens here
 /// </summary>

    #pragma warning disable 1591
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieDbContext _context;

        public FranchiseService(MovieDbContext context)
        {
            _context = context;
        }

        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.FranchiseId == id);
        }

        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchises.Include(c => c.Movies).ToListAsync();
        }

        public async Task<IEnumerable<Character>> GetCharactersInFranchiseAsync(int id)
        {
            return await _context.Character.Where(character => character.Movies.Any(movie => movie.FranchiseId == id)).Include(c => c.Movies).ToListAsync();
        }

        public async Task<IEnumerable<Movie>> GetMoviesInFranchiseAsync(int id)
        {
            return await _context.Movies.Where(c => c.FranchiseId == id).ToListAsync();
        }

        public async Task<Franchise> GetSpecificFranchiseAsync(int id)
        {
            return await _context.Franchises.Include(c => c.Movies).FirstAsync(c => c.FranchiseId == id);
        }

        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateFranchiseMoviesAsync(int franchiseId, List<int> movies)
        {
            Franchise FranchiseToUpdateMovies = await _context.Franchises
                .Include(c => c.Movies)
                .Where(c => c.FranchiseId == franchiseId)
                .FirstAsync();


            List<Movie> _movies = new();
            foreach (int movieId in movies)
            {
                Movie? movie = await _context.Movies.FindAsync(movieId);
                if (movie == null)
                {
                    throw new KeyNotFoundException();
                }

                _movies.Add(movie);
            }
            FranchiseToUpdateMovies.Movies = _movies;
            await _context.SaveChangesAsync();
        }
    }
#pragma warning restore 1591
}
