
using movies_assignment.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace movies_assignment.Services
{/// <summary>
 /// Services are used so controllers have less actions. Database interaction happens here
 /// </summary>
    public interface IMovieService
    {
        /// <summary>
        /// Asynchronous method to get all movies from database
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();

        /// <summary>
        /// Asynchronous method to get specific movie from database
        /// </summary>
        /// <param name="id">ID of the movie</param>
        /// <returns></returns>
        public Task<Movie> GetSpecificMovieAsync(int id);

        /// <summary>
        /// Asynchronous method to add <paramref name="movie"/> to database
        /// </summary>
        /// <param name="movie">Movie to add</param>
        /// <returns></returns>
        public Task<Movie> AddMovieAsync(Movie movie);

        /// <summary>
        /// Asynchronous method to update <paramref name="movie"/> in database
        /// </summary>
        /// <param name="movie">Updated movie</param>
        /// <returns></returns>
        public Task UpdateMovieAsync(Movie movie);

        /// <summary>
        /// Asynchronous method to update <paramref name="characters"/> the movie has
        /// </summary>
        /// <param name="movieID"></param>
        /// <param name="characters"></param>
        /// <returns></returns>
        public Task UpdateMovieCharactersAsync(int movieID, List<int> characters);

        /// <summary>
        /// Asynchronous method to delete movie from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task DeleteMovieAsync(int id);

        /// <summary>
        /// Check if movie exists in database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool MovieExists(int id);

        /// <summary>
        /// Get all characters in a movie
        /// </summary>
        /// <param name="id">ID of the movie</param>
        /// <returns></returns>
        public Task<IEnumerable<Character>> GetAllCharactersAsync(int id); 
    }
}
