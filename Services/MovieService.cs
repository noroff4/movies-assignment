using Microsoft.EntityFrameworkCore;
using movies_assignment.Models;

namespace movies_assignment.Services
{/// <summary>
 /// Services are used so controllers have less actions. Database interaction happens here
 /// </summary>
#pragma warning disable 1591
    public class MovieService : IMovieService
    {

        private readonly MovieDbContext _context;

        public MovieService(MovieDbContext context)
        {
            _context = context;
        }

        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.MovieId == id);
        }

        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies.Include(c => c.Characters).ToListAsync();
        }

        public async Task<IEnumerable<Character>> GetAllCharactersAsync(int id)
        {

            return await _context.Character.Where(character => character.Movies.Any(movie => movie.MovieId == id)).Include(character => character.Movies).ToListAsync();

        }

        public async Task<Movie> GetSpecificMovieAsync(int id)
        {
            return await _context.Movies.Include(c => c.Characters).FirstAsync(c => c.MovieId == id);

        }

        public async Task UpdateMovieCharactersAsync(int movieID, List<int> characters)
        {

            Movie MovieToUpdateCharacters = await _context.Movies
                .Include(c => c.Characters)
                .Where(c => c.MovieId == movieID)
                .FirstAsync();


            List<Character> _characters = new();
            foreach (int characterId in characters)
            {
                Character? character = await _context.Character.FindAsync(characterId);
                if (character == null)
                {
                    throw new KeyNotFoundException();
                }

                _characters.Add(character);
            }
            MovieToUpdateCharacters.Characters = _characters;
            await _context.SaveChangesAsync();
        }

        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
#pragma warning restore 1591
}
