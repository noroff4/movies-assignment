﻿using movies_assignment.Models;

namespace movies_assignment.Services
{/// <summary>
 /// Services are used so controllers have less actions. Database interaction happens here
 /// </summary>
    public interface IFranchiseService
    {
        /// <summary>
        /// Asynchronous method to get all franchises from database
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();

        /// <summary>
        /// Asynchronous method to get specific franchise from database
        /// </summary>
        /// <param name="id">ID of the franchise</param>
        /// <returns></returns>
        public Task<Franchise> GetSpecificFranchiseAsync(int id);

        /// <summary>
        /// Asynchronous method to add <paramref name="franchise"/> to database
        /// </summary>
        /// <param name="franchise">Franchise to add</param>
        /// <returns></returns>
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);

        /// <summary>
        /// Asynchronous method to update <paramref name="franchise"/> in database
        /// </summary>
        /// <param name="franchise">Updated franchise</param>
        /// <returns></returns>
        public Task UpdateFranchiseAsync(Franchise franchise);

        /// <summary>
        /// Asynchronous method to update movies the franchise has
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="movies"></param>
        /// <returns></returns>
        public Task UpdateFranchiseMoviesAsync(int franchiseId, List<int> movies);

        /// <summary>
        /// Asynchronous method to delete franchise from database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task DeleteFranchiseAsync(int id);

        /// <summary>
        /// Check if franchise exists in database
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool FranchiseExists(int id);

        /// <summary>
        /// Asynchronous method to get all movies in specific franchise
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <returns></returns>
        public Task<IEnumerable<Movie>> GetMoviesInFranchiseAsync(int id);

        /// <summary>
        /// Asynchronous method to get all characters in a franchise
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <returns></returns>
        public Task<IEnumerable<Character>> GetCharactersInFranchiseAsync(int id);
    }
}
