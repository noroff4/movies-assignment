# movies-assignment


This project is an ASP.NET Core web API that uses Entity Framework code first database. User can do basic CRUD-opearations to movies, franchises and characters and add relations between them.
<br>
<br>

## Features
- Basic CRUD for:
    - Movies
    - Characters
    - Franchises
- Get all movies in franchise
- Get all characters in franchise
- Edit characters in movie
- Edit movies in character
- Edit movies in franchise
<br>


## Installation
First you need to install SQL Server and SQL Server Management Studio. Also you need to clone or download the project to your computer.

To clone the project run in console:
```
git clone https://gitlab.com/noroff4/movies-assignment.git

```
 - Open solution in visual studio
 - Edit connection string inside appsettings.json to include your SQL server name
 ```
 "DefaultConnection": "server=SERVER_NAME_HERE;Database=Movies;trusted_connection=true;MultipleActiveResultSets=true"
 ```
 - Open Package Manager Console and run the following command:
 ```
 Update-Database
 ```
<br>
<br>

## Usage
After installation steps, you can run the application and use swagger to test endpoints
<br>

## Contributors
- Leevi Peltonen https://gitlab.com/leevi-peltonen
- Marie Puhakka https://gitlab.com/mariesusan
<br>


## License
[MIT](https://choosealisence.com/licenses/mit/)

