﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using movies_assignment.DTOs.CharacterDTOs;
using movies_assignment.Models;
using movies_assignment.Services;

namespace movies_assignment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;
        private readonly ICharacterService _characterService;
        public CharactersController(MovieDbContext context, IMapper mapper, ICharacterService characterService)
        {
            _context = context;
            _mapper = mapper;
            _characterService = characterService;
        }
        /// <summary>
        /// Gets all characters
        /// </summary>
        /// <returns></returns>
        // GET: api/Characters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacter()
        { 
            return _mapper.Map<List<CharacterReadDTO>>(await _characterService.GetAllCharactersAsync());
        }
        /// <summary>
        /// Get a character by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Characters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            var character = await _characterService.GetSpecificCharacterAsync(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }


        /// <summary>
        /// Edit a character
        /// </summary>
        /// <param name="id">Character to edit</param>
        /// <param name="characterEditDto">New data for character</param>
        /// <returns></returns>
        // PUT: api/Characters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, [FromBody] CharacterEditDTO characterEditDto)
        {
            if (id != characterEditDto.CharacterId)
            {
                return BadRequest();
            }

            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }

            var character = _mapper.Map<Character>(characterEditDto);
            await _characterService.UpdateCharacterAsync(character);

            return NoContent();
        }


        /// <summary>
        /// Set character movies
        /// </summary>
        /// <param name="id">Id of the character</param>
        /// <param name="movies">Id's of the movies for the character</param>
        /// <returns></returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateCharacterMovies(int id, List<int> movies)
        {
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }

            try
            {
                await _characterService.UpdateCharacterMoviesAsync(id, movies);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid certification.");
            }

            return NoContent();
        }



        /// <summary>
        /// Update specific data for character
        /// </summary>
        /// <param name="id">Id of the character to update</param>
        /// <param name="patchDoc"></param>
        /// <returns></returns>
        // PATCH
        [HttpPatch("{id}")]
        public async Task<IActionResult> UpdateFranchise(int id, [FromBody] JsonPatchDocument<Character> patchDoc)
        {
            if (patchDoc != null)
            {
                var character = _context.Character.FirstOrDefault(f => f.CharacterId == id);
                patchDoc.ApplyTo(character, ModelState);

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                await _context.SaveChangesAsync();
                return new ObjectResult(character);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        /// <summary>
        /// Adds new character
        /// </summary>
        /// <param name="characterCreateDto">New data for character</param>
        /// <returns></returns>
        // POST: api/Characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter([FromBody] CharacterCreateDTO characterCreateDto)
        {

            var character = _mapper.Map<Character>(characterCreateDto);
            character = await _characterService.AddCharacterAsync(character);

            return CreatedAtAction("GetCharacter", new { id = character.CharacterId }, character);
        }


        /// <summary>
        /// Deletes character
        /// </summary>
        /// <param name="id">Id of the character to delete</param>
        /// <returns></returns>
        // DELETE: api/Characters/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {

            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }

            await _characterService.DeleteCharacterAsync(id);

            return NoContent();
        }
    
    }
}
