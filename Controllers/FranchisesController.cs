﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.NewtonsoftJson;
using movies_assignment.Models;
using AutoMapper;
using movies_assignment.DTOs.FranchiseDTOs;
using movies_assignment.DTOs.CharacterDTOs;
using movies_assignment.Services;
using movies_assignment.DTOs.MovieDTOs;

namespace movies_assignment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;
        public FranchisesController(MovieDbContext context, IMapper mapper, IFranchiseService franchiseService)
        {
            _context = context;
            _mapper = mapper;
            _franchiseService = franchiseService;
        }


        /// <summary>
        /// Get all franchises
        /// </summary>
        /// <returns></returns>
        // GET: api/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllFranchisesAsync());
        }


        /// <summary>
        /// Get franchise by id
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <returns></returns>
        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            var franchise = await _franchiseService.GetSpecificFranchiseAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }



        /// <summary>
        /// Update a franchise
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <param name="franchiseEditDto">Data of the updated franchise</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, [FromBody] FranchiseEditDTO franchiseEditDto)
        {



            if (id != franchiseEditDto.FranchiseId)
            {
                return BadRequest();
            }

            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            var franchise = _mapper.Map<Franchise>(franchiseEditDto);
            await _franchiseService.UpdateFranchiseAsync(franchise);

            return NoContent();
        }


        /// <summary>
        /// Update specific information of a franchise
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <param name="patchDoc"></param>
        /// <returns></returns>
        // PATCH: api/Franchises/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPatch("{id}")]
        public async Task<IActionResult> UpdateFranchise(int id, [FromBody] JsonPatchDocument<FranchiseEditDTO> patchDoc)
        {
            if (patchDoc != null)
            {
                var franchise = _context.Franchises.FirstOrDefault(f => f.FranchiseId == id);
                var franchiseEdit = _mapper.Map<FranchiseEditDTO>(franchise);
                patchDoc.ApplyTo(franchiseEdit, ModelState);

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                await _context.SaveChangesAsync();
                return new ObjectResult(franchiseEdit);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }


        /// <summary>
        /// Create new franchise
        /// </summary>
        /// <param name="franchiseCreateDto">Data of the franchise</param>
        /// <returns></returns>
        // POST: api/Franchises
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise([FromBody] FranchiseCreateDTO franchiseCreateDto)
        {
            var franchise = _mapper.Map<Franchise>(franchiseCreateDto);
            franchise = await _franchiseService.AddFranchiseAsync(franchise);

            return CreatedAtAction("GetFranchise", new { id = franchise.FranchiseId }, franchise);
        }

        /// <summary>
        /// Delete a franchise
        /// </summary>
        /// <param name="id">ID of the franchise</param>
        /// <returns></returns>
        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {

            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            await _franchiseService.DeleteFranchiseAsync(id);

            return NoContent();
        }


        /// <summary>
        /// Add movies to franchise
        /// </summary>
        /// <param name="id">Id of the franchise</param>
        /// <param name="movies">List of id's of the movies</param>
        /// <returns></returns>
        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateFranchiseMovies(int id, List<int> movies)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            try
            {
                await _franchiseService.UpdateFranchiseMoviesAsync(id, movies);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid certification.");
            }

            return NoContent();
        }


        /// <summary>
        /// Gets all movies in a franchise
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <returns></returns>
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMoviesInFranchise(int id)
        {

            var franchise = await _franchiseService.GetMoviesInFranchiseAsync(id);
            return _mapper.Map<List<MovieReadDTO>>(franchise);
        }

        /// <summary>
        /// Gets all characters in a franchise
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <returns></returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharactersInFranchise(int id)
        {
            var characters = await _franchiseService.GetCharactersInFranchiseAsync(id);
            return _mapper.Map<List<CharacterReadDTO>>(characters);
        }
    }
}
