﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using movies_assignment.DTOs.CharacterDTOs;
using movies_assignment.DTOs.MovieDTOs;
using movies_assignment.Models;
using movies_assignment.Services;

namespace movies_assignment.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;

        public MoviesController(MovieDbContext context, IMapper mapper, IMovieService movieService)
        {
            _context = context;
            _mapper = mapper;
            _movieService = movieService;
        }


        /// <summary>
        /// Gets all movies
        /// </summary>
        /// <returns></returns>
        // GET: api/Movies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetAllMoviesAsync());
        }


        /// <summary>
        /// Gets a specific movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            var movie = await _movieService.GetSpecificMovieAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Add characters to a movie
        /// </summary>
        /// <param name="id">ID of the movie</param>
        /// <param name="characters">List of id's of characters</param>
        /// <returns></returns>
        [HttpPut("{id}/characters")]
        public async Task<IActionResult> UpdateMovieCharacters(int id, List<int> characters)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            try
            {
                await _movieService.UpdateMovieCharactersAsync(id, characters);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Invalid certification.");
            }

            return NoContent();
        }

        /// <summary>
        /// Edit a movie
        /// </summary>
        /// <param name="id">ID of the movie</param>
        /// <param name="movieEditDto">Data of the edited movie</param>
        /// <returns></returns>
        // PUT: api/Movies/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, [FromBody] MovieEditDTO movieEditDto)
        {
            if (id != movieEditDto.MovieId)
            {
                return BadRequest();
            }

            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            var movie = _mapper.Map<Movie>(movieEditDto);
            await _movieService.UpdateMovieAsync(movie);

            return NoContent();

        }


        /// <summary>
        /// Update specific data from movie
        /// </summary>
        /// <param name="id">ID of the movie</param>
        /// <param name="patchDoc"></param>
        /// <returns></returns>
        //PATCH
        [HttpPatch("{id}")]
        public async Task<IActionResult> UpdateFranchise(int id, [FromBody] JsonPatchDocument<Movie> patchDoc)
        {
            if (patchDoc != null)
            {
                var movie = _context.Movies.FirstOrDefault(f => f.MovieId == id);
                patchDoc.ApplyTo(movie, ModelState);

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                await _context.SaveChangesAsync();
                return new ObjectResult(movie);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }


        /// <summary>
        /// Create new movie
        /// </summary>
        /// <param name="movieCreateDto">Data of the new movie</param>
        /// <returns></returns>
        // POST: api/Movies
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie([FromBody] MovieCreateDTO movieCreateDto)
        {
            var movie = _mapper.Map<Movie>(movieCreateDto);
            movie = await _movieService.AddMovieAsync(movie);

            return CreatedAtAction("GetMovie", new { id = movie.MovieId }, movie);
        }
        /// <summary>
        /// Delete movie
        /// </summary>
        /// <param name="id">ID of the movie</param>
        /// <returns></returns>
        // DELETE: api/Movies/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            await _movieService.DeleteMovieAsync(id);

            return NoContent();
        }
        /// <summary>
        /// Get all characters of a movie
        /// </summary>
        /// <param name="id">Id of the movie</param>
        /// <returns></returns>
        [HttpGet("{id}/characters")]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetAllCharactersInMovie(int id)
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _movieService.GetAllCharactersAsync(id));
        }
    }
}
