﻿using movies_assignment.Models;
using System.ComponentModel.DataAnnotations;

namespace movies_assignment.DTOs.MovieDTOs
{
    public class MovieReadDTO
    {
        public int MovieId { get; set; }
        [MaxLength(64)]
        public string Title { get; set; }
        [MaxLength(255)]
        public string? Genre { get; set; }
        public int ReleaseYear { get; set; }
        [MaxLength(255)]
        
        public string? Director { get; set; }
        [MaxLength(255)]
        public string? Picture { get; set; }
        [MaxLength(255)]
        public string? Trailer { get; set; }

        public int FranchiseId { get; set; }
        
        public List<int>? Characters { get; set; }
    }
}
