using movies_assignment.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace movies_assignment.DTOs.FranchiseDTOs
{
    public class FranchiseEditDTO
    {
        public int FranchiseId { get; set; }
        [MaxLength(255)]
        public string Name { get; set; }
        [MaxLength(255)]
        public string Description { get; set; }

    }
}