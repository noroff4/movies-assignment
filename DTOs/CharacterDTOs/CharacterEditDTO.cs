using movies_assignment.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace movies_assignment.DTOs.CharacterDTOs
{
    public class CharacterEditDTO
    {
        public int CharacterId { get; set; }
        [MaxLength(255)]
        public string FullName { get; set; }
        [MaxLength(255)]
        public string Alias { get; set; }
        [MaxLength(64)]
        public string Gender { get; set; }
        [MaxLength(255)]
        public string? PictureURL { get; set; }
    }
}