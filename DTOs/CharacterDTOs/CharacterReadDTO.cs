﻿using movies_assignment.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace movies_assignment.DTOs.CharacterDTOs
{
    public class CharacterReadDTO
    {
        public int CharacterId { get; set; }
        [MaxLength(255)]
        public string FullName { get; set; }
        [MaxLength(255)]
        public string Alias { get; set; }
        [MaxLength(64)]
        public string Gender { get; set; }
        [MaxLength(255)]
        public string? PictureURL { get; set; }
        public List<int>? Movies { get; set; }
    }
}
